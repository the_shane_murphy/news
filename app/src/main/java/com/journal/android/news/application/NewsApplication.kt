package com.journal.android.news.application

import android.app.Activity
import android.app.Application
import android.content.Context
import com.journal.android.news.application.builder.ApplicationComponent
import com.journal.android.news.application.builder.ApplicationModule
import com.journal.android.news.application.builder.DaggerApplicationComponent
import com.journal.android.news.network.builder.NetworkModule


class NewsApplication : Application() {

  private var applicationComponent: ApplicationComponent? = null

  override fun onCreate() {
    super.onCreate()
    createAplicationComponent()
  }

  companion object {
    fun component(context: Context): ApplicationComponent? {
      val applicationInstance = (context as Activity).application
      return NewsApplication::class.java.cast(applicationInstance).applicationComponent
    }
  }

  private fun createAplicationComponent() {
    applicationComponent = DaggerApplicationComponent.builder()
        .applicationModule(ApplicationModule(this))
        .networkModule(NetworkModule())
        .build()
    applicationComponent!!.inject(this)
  }
}