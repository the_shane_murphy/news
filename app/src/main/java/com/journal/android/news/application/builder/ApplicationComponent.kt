package com.journal.android.news.application.builder

import com.journal.android.news.application.NewsApplication
import com.journal.android.news.network.builder.NetworkModule
import dagger.Component
import okhttp3.OkHttpClient

@ApplicationScope
@Component(modules = arrayOf(ApplicationModule::class, NetworkModule::class))
interface ApplicationComponent {

  fun inject(application: NewsApplication)

  fun okHttpClient(): OkHttpClient
}