package com.journal.android.news.application.builder

import android.app.Application
import android.content.Context
import dagger.Module
import dagger.Provides


@Module
@ApplicationScope
class ApplicationModule(private val application: Application) {

  @ApplicationScope
  @Provides
  fun provideApplicationContext(): Context = application.applicationContext

  @ApplicationScope
  @Provides
  fun provideApplication(): Application = application
}