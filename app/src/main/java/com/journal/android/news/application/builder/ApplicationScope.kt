package com.journal.android.news.application.builder

import javax.inject.Scope
import kotlin.annotation.AnnotationRetention.RUNTIME


@Scope
@Retention(RUNTIME)
annotation class ApplicationScope