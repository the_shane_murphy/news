package com.journal.android.news.models


data class Article(val title: String,
    val images: Images?,
    val type: String)