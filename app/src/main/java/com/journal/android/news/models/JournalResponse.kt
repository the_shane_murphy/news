package com.journal.android.news.models


data class JournalResponse(val rendered: Int,
    val status: Boolean,
    val response: Response,
    val version: String,
    val request: Request)
