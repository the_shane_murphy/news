package com.journal.android.news.models


data class Response(val articles: List<Article>)