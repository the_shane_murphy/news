package com.journal.android.news.models


data class Thumbnail(val image: String,
    val width: Int,
    val height: Int)