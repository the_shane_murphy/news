package com.journal.android.news.models.deserializers

import com.google.gson.Gson
import com.google.gson.JsonDeserializationContext
import com.google.gson.JsonDeserializer
import com.google.gson.JsonElement
import com.journal.android.news.models.Images
import java.lang.reflect.Type


class ImagesDeserializer : JsonDeserializer<Images> {
  override fun deserialize(json: JsonElement?, typeOfT: Type?, context: JsonDeserializationContext?): Images {
    if (json != null) {
      if (json.isJsonArray) {
        return Images(null)
      }
      return Gson().fromJson(json, Images::class.java)
    }
    throw RuntimeException("Exception parsing Images: field is null")
  }
}