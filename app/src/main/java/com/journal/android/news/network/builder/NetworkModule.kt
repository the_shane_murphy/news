package com.journal.android.news.network.builder

import android.app.Application
import com.journal.android.news.BuildConfig
import com.journal.android.news.application.builder.ApplicationScope
import dagger.Module
import dagger.Provides
import okhttp3.Cache
import okhttp3.OkHttpClient
import okhttp3.OkHttpClient.Builder
import okhttp3.logging.HttpLoggingInterceptor
import java.io.File
import java.util.concurrent.TimeUnit.SECONDS


@Module
class NetworkModule {

  companion object {
    private val HTTP_CACHE_DIR_NAME = "okhttp_cache"
    private val DISK_CACHE_SIZE = 50 * 1024 * 1024 // 50MB
    private val CONNECTION_TIMEOUT_SECONDS = 10
  }

  @ApplicationScope
  @Provides
  fun okHttpClient(application: Application): OkHttpClient {
    val builder = createOkHttpClient(application)
    if (BuildConfig.DEBUG) {
      addLoggingInterceptor(builder)
    }
    return builder.build()
  }

  private fun createOkHttpClient(application: Application): OkHttpClient.Builder {
    // Install an HTTP cache in the application cache directory.
    val cacheDir = File(application.cacheDir, HTTP_CACHE_DIR_NAME)
    val cache = Cache(cacheDir, DISK_CACHE_SIZE.toLong())

    return Builder().connectTimeout(CONNECTION_TIMEOUT_SECONDS.toLong(), SECONDS)
        .readTimeout(CONNECTION_TIMEOUT_SECONDS.toLong(), SECONDS)
        .writeTimeout(CONNECTION_TIMEOUT_SECONDS.toLong(), SECONDS)
        .cache(cache)
  }

  private fun addLoggingInterceptor(builder: OkHttpClient.Builder) {
    val httpLoggingInterceptor = HttpLoggingInterceptor()
    httpLoggingInterceptor.level = HttpLoggingInterceptor.Level.BODY
    builder.addInterceptor(httpLoggingInterceptor)
  }
}