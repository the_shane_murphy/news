package com.journal.android.news.network.rest

import com.journal.android.news.models.JournalResponse
import io.reactivex.Flowable
import retrofit2.http.GET
import retrofit2.http.Path


interface JournalService {
  @GET("/v3/sample/thejournal")
  fun fetchArticles(): Flowable<JournalResponse>

  @GET("/v3/sample/tag/{slug}")
  fun fetchTaggedArticles(@Path(value = "slug") tag: String): Flowable<JournalResponse>
}