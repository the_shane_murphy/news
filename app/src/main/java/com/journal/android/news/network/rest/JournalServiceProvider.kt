package com.journal.android.news.network.rest

import android.content.Context
import com.google.gson.GsonBuilder
import com.journal.android.news.R
import com.journal.android.news.models.Images
import com.journal.android.news.models.deserializers.ImagesDeserializer
import com.journal.android.news.utilities.Constants
import okhttp3.Credentials
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.Response
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.IOException
import java.nio.charset.StandardCharsets


class JournalServiceProvider(val context: Context, val okHttpClient: OkHttpClient) {
  private val customGson = GsonBuilder().registerTypeAdapter(Images::class.java, ImagesDeserializer()).create()

  fun provideJournalService(): JournalService {
    val retrofit = Retrofit.Builder()
        .baseUrl(context.getString(R.string.journal_api_endpoint))
        .client(okHttpClient.newBuilder()
            .addInterceptor(BasicAuthInterceptor("sample", "eferw5wr335£65"))
            .build())
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .addConverterFactory(GsonConverterFactory.create(customGson))
        .build()
    return retrofit.create(JournalService::class.java)
  }

  inner class BasicAuthInterceptor(user: String, password: String) : Interceptor {

    private val credentials: String = Credentials.basic(user, password, StandardCharsets.UTF_8)

    @Throws(IOException::class)
    override fun intercept(chain: Interceptor.Chain): Response {
      val request = chain.request()
      val authenticatedRequest = request.newBuilder().header(Constants.AUTH, credentials).build()
      return chain.proceed(authenticatedRequest)
    }

  }
}