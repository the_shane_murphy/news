package com.journal.android.news.ui.base


interface BasePresenter {
  fun load()
  fun finish()
}