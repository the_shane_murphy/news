package com.journal.android.news.ui.base

import android.view.View


interface BaseView {
  fun getView(): View
}