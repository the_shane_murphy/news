package com.journal.android.news.ui.base


interface ToolbarView : BaseView {
  fun setToolbarTitle(title: String)
}