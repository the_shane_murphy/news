package com.journal.android.news.ui.home

import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.journal.android.news.application.NewsApplication
import com.journal.android.news.ui.home.builder.DaggerHomeComponent
import com.journal.android.news.ui.home.builder.HomeModule
import com.journal.android.news.ui.home.core.presenter.DefaultHomePresenter
import com.journal.android.news.ui.home.core.view.HomeView
import javax.inject.Inject


class HomeActivity : AppCompatActivity() {
  @Inject
  lateinit var view: HomeView

  @Inject
  lateinit var presenter: DefaultHomePresenter

  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)

    //DI
    DaggerHomeComponent.builder()
        .applicationComponent(NewsApplication.component(this))
        .homeModule(HomeModule(this))
        .build()
        .inject(this)

    setContentView(view.getView())
    presenter.load()
  }

  override fun onDestroy() {
    super.onDestroy()
    presenter.finish()
  }
}