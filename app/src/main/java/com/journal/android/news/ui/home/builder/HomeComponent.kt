package com.journal.android.news.ui.home.builder

import com.journal.android.news.application.builder.ApplicationComponent
import com.journal.android.news.ui.home.HomeActivity
import dagger.Component

@HomeScope
@Component(modules = arrayOf(HomeModule::class), dependencies = arrayOf(ApplicationComponent::class))
interface HomeComponent {

  fun inject(activity: HomeActivity)
}