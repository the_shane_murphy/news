package com.journal.android.news.ui.home.builder

import android.content.Context
import com.journal.android.news.network.rest.JournalServiceProvider
import com.journal.android.news.ui.home.HomeActivity
import com.journal.android.news.ui.home.core.interactor.DefaultHomeInteractor
import com.journal.android.news.ui.home.core.interactor.HomeInteractor
import com.journal.android.news.ui.home.core.presenter.DefaultHomePresenter
import com.journal.android.news.ui.home.core.view.DefaultHomeView
import com.journal.android.news.ui.home.core.view.HomeView
import dagger.Module
import dagger.Provides
import okhttp3.OkHttpClient

@Module
class HomeModule(private val activity: HomeActivity) {

  @Provides
  @HomeScope
  fun provideView(): HomeView = DefaultHomeView(activity)

  @Provides
  @HomeScope
  fun provideInteractor(journalService: JournalServiceProvider): HomeInteractor = DefaultHomeInteractor(journalService)

  @Provides
  @HomeScope
  fun provideServiceProvider(okHttpClient: OkHttpClient) = JournalServiceProvider(context = activity, okHttpClient = okHttpClient)

  @Provides
  @HomeScope
  fun providePresenter(view: HomeView, interactor: HomeInteractor) =
      DefaultHomePresenter(view, interactor)

  @Provides
  @HomeScope
  fun provideContext(): Context = activity
}