package com.journal.android.news.ui.home.core.interactor

import com.journal.android.news.network.rest.JournalService
import com.journal.android.news.network.rest.JournalServiceProvider


class DefaultHomeInteractor(journalService: JournalServiceProvider) : HomeInteractor {
  private val service: JournalService = journalService.provideJournalService()

  override fun fetchArticles() = service.fetchArticles()

  override fun fetchTaggedArticles(tag: String) = service.fetchTaggedArticles(tag)
}