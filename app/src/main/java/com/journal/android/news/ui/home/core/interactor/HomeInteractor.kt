package com.journal.android.news.ui.home.core.interactor

import com.journal.android.news.models.JournalResponse
import io.reactivex.Flowable


interface HomeInteractor {
  fun fetchArticles(): Flowable<JournalResponse>
  fun fetchTaggedArticles(tag: String): Flowable<JournalResponse>
}