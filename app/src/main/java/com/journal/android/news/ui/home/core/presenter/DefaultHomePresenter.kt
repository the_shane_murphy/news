package com.journal.android.news.ui.home.core.presenter

import com.journal.android.news.models.JournalResponse
import com.journal.android.news.ui.base.BasePresenter
import com.journal.android.news.ui.home.core.interactor.HomeInteractor
import com.journal.android.news.ui.home.core.view.HomeView
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.disposables.CompositeDisposable
import io.reactivex.disposables.Disposable
import io.reactivex.schedulers.Schedulers


class DefaultHomePresenter(private val view: HomeView,
    private val interactor: HomeInteractor) : BasePresenter {

  private val disposable = CompositeDisposable()

  override fun load() {
    disposable.add(fetchArticles())
  }

  override fun finish() {
    disposable.clear()
  }

  private fun fetchArticles(): Disposable {
    return interactor.fetchArticles()
        .doOnError({ handleError(it) })
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnNext({ view.showLoading() })
        .subscribe({ handleSuccess(it) },
            { handleError(it) },
            { view.hideLoading() })
  }

  private fun handleSuccess(journalResponse: JournalResponse) {
    view.showArticles(journalResponse.response.articles)
  }

  private fun handleError(throwable: Throwable) {
    view.showError()
  }
}