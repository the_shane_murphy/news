package com.journal.android.news.ui.home.core.view

import android.content.Context
import android.support.v7.app.AlertDialog
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView
import android.support.v7.widget.Toolbar
import android.util.AttributeSet
import android.view.View
import android.widget.LinearLayout
import android.widget.ProgressBar
import com.journal.android.news.R
import com.journal.android.news.models.Article
import com.journal.android.news.ui.home.core.view.adapter.HomeAdapter


class DefaultHomeView : HomeView, LinearLayout {

  private var toolbar: Toolbar? = null
  private var list: RecyclerView? = null
  private var loadingIndicator: ProgressBar? = null

  private var adapter: HomeAdapter? = null

  constructor(context: Context) : this(context, null)

  constructor(context: Context, attrs: AttributeSet?) : this(context, attrs, 0)

  constructor(context: Context, attrs: AttributeSet?, defStyleAttr: Int) : super(context, attrs, defStyleAttr) {
    inflate(context, R.layout.view_home, this)
    toolbar = findViewById(R.id.toolbar)
    list = findViewById(R.id.recycler_view)
    loadingIndicator = findViewById(R.id.progress_bar)
    setToolbarTitle(context.getString(R.string.home))
  }

  override fun getView(): View = this

  override fun setToolbarTitle(title: String) {
    toolbar?.title = title
  }

  override fun showLoading() {
    loadingIndicator?.visibility = View.VISIBLE
  }

  override fun hideLoading() {
    loadingIndicator?.visibility = View.GONE
  }

  override fun showArticles(articles: List<Article>) {
    adapter = HomeAdapter(articles)
    list?.layoutManager = LinearLayoutManager(context)
    list?.adapter = adapter
  }

  override fun showError() {
    AlertDialog.Builder(context)
        .setCancelable(false)
        .setTitle(R.string.error)
        .setMessage(R.string.error_retrieval_failed)
        .setNeutralButton(android.R.string.ok, { dialogInterface, _ -> dialogInterface.dismiss() })
        .create()
        .show()
  }
}