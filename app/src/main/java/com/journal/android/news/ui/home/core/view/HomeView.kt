package com.journal.android.news.ui.home.core.view

import com.journal.android.news.models.Article
import com.journal.android.news.ui.base.ToolbarView


interface HomeView : ToolbarView {
  fun showLoading()
  fun hideLoading()
  fun showArticles(articles: List<Article>)
  fun showError()
}