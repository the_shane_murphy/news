package com.journal.android.news.ui.home.core.view.adapter

import android.support.v7.widget.RecyclerView
import android.view.ViewGroup
import com.journal.android.news.models.Article
import com.journal.android.news.ui.home.core.view.adapter.viewholders.ArticleViewHolder
import com.journal.android.news.utilities.inflate


class HomeAdapter(private val articles: List<Article>) : RecyclerView.Adapter<ArticleViewHolder>() {

  override fun onBindViewHolder(holder: ArticleViewHolder, position: Int) {
    val article = articles[position]
    holder.setTitle(article.title)
    holder.setImages(article.images)
  }

  override fun getItemCount() = articles.size

  override fun onCreateViewHolder(parent: ViewGroup, viewType: Int): ArticleViewHolder =
      ArticleViewHolder(parent.inflate(ArticleViewHolder.getLayoutToInflate()))
}