package com.journal.android.news.ui.home.core.view.adapter.viewholders

import android.support.v7.widget.RecyclerView
import android.view.View
import android.widget.ImageView
import android.widget.TextView
import com.journal.android.news.R
import com.journal.android.news.models.Images
import com.squareup.picasso.Picasso


class ArticleViewHolder(private val articleItem: View) : RecyclerView.ViewHolder(articleItem) {

  private val titleView: TextView = articleItem.findViewById(R.id.title)
  private val thumbnailView: ImageView = articleItem.findViewById(R.id.thumbnail)

  fun setTitle(title: String) {
    titleView.text = title
  }

  fun setImages(images: Images?) {
    val thumbnail = images?.thumbnail
    Picasso.with(articleItem.context).load(thumbnail?.image).into(thumbnailView)
  }

  companion object {
    fun getLayoutToInflate() = R.layout.view_article_item
  }

}