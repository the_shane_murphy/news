package com.journal.android.news.ui.home.core.presenter

import com.journal.android.news.models.Article
import com.journal.android.news.models.JournalResponse
import com.journal.android.news.ui.home.core.interactor.HomeInteractor
import com.journal.android.news.ui.home.core.view.HomeView
import com.journal.android.news.utilities.TestUtils
import io.reactivex.Flowable
import io.reactivex.android.plugins.RxAndroidPlugins
import io.reactivex.schedulers.Schedulers
import org.junit.After
import org.junit.AfterClass
import org.junit.Before
import org.junit.BeforeClass
import org.junit.Test
import org.mockito.Mockito
import org.mockito.Mockito.anyList
import org.mockito.Mockito.mock
import org.mockito.Mockito.never
import org.mockito.Mockito.times
import org.mockito.Mockito.verify
import org.mockito.Mockito.verifyNoMoreInteractions


class DefaultHomePresenterTest {

  private lateinit var view: HomeView
  private lateinit var interactor: HomeInteractor
  private lateinit var presenter: DefaultHomePresenter

  companion object {
    @BeforeClass
    @JvmStatic
    fun setUpClass() {

      // Override the default "out of the box" AndroidSchedulers.mainThread() Scheduler
      //
      // This is necessary here because otherwise if the static initialization block in AndroidSchedulers
      // is executed before this, then the Android SDK dependent version will be provided instead.
      //
      // This would cause a java.lang.ExceptionInInitializerError when running the test as a
      // Java JUnit test as any attempt to resolve the default underlying implementation of the
      // AndroidSchedulers.mainThread() will fail as it relies on unavailable Android dependencies.

      // Comment out this line to see the java.lang.ExceptionInInitializerError
      RxAndroidPlugins.setInitMainThreadSchedulerHandler { _ -> Schedulers.trampoline() }
    }

    @AfterClass
    @JvmStatic
    fun tearDownClass() {
      // Not strictly necessary because we can't reset the value set by setInitMainThreadSchedulerHandler,
      // but it doesn't hurt to clean up anyway.
      RxAndroidPlugins.reset()
    }
  }

  @Before
  fun setUp() {
    view = mock(HomeView::class.java)
    interactor = mock(HomeInteractor::class.java)
    presenter = DefaultHomePresenter(view, interactor)
  }

  @After
  fun tearDown() {
    presenter.finish()
  }

  @Test
  fun load_shouldShowArticles_whenResponseIsCorrect() {
    //given
    val apiResponse = TestUtils.readJson("successful_journal_response.json", JournalResponse::class.java)
    val articles: List<Article> = apiResponse.response.articles
    Mockito.`when`(interactor.fetchArticles()).thenReturn(Flowable.just(apiResponse))

    //when
    presenter.load()

    //then
    verify(interactor).fetchArticles()
    verify(view, times(1)).showLoading()
    verify(view, times(1)).showArticles(articles)
    verify(view, times(1)).hideLoading()
    verify(view, never()).showError()
    verifyNoMoreInteractions(view)
    verifyNoMoreInteractions(interactor)
  }

  @Test
  fun load_shouldShowError_whenResponseContainsBadData() {
    //given
    val apiResponse =
        TestUtils.readJson("successful_journal_response_with_missing_response_field.json", JournalResponse::class.java)
    Mockito.`when`(interactor.fetchArticles()).thenReturn(Flowable.just(apiResponse))

    //when
    presenter.load()

    //then
    verify(interactor).fetchArticles()
    verify(view, times(1)).showLoading()
    verify(view, times(1)).showError()
    verify(view, never()).showArticles(anyList())
    verifyNoMoreInteractions(interactor)
  }
}