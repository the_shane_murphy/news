package com.journal.android.news.utilities

import com.google.gson.Gson
import java.io.BufferedReader
import java.io.File
import java.io.FileInputStream
import java.io.IOException
import java.io.InputStreamReader
import java.nio.charset.StandardCharsets


object TestUtils {

  fun <T> readJson(path: String, classOfT: Class<T>): T {
    val content = readJson(path)
    return Gson().fromJson<T>(content, classOfT)
  }

  private fun readJson(path: String): String {
    val resource = TestUtils::class.java.classLoader.getResource(path)
    val file = File(resource.path)
    val content = StringBuilder()
    try {
      BufferedReader(InputStreamReader(FileInputStream(file), StandardCharsets.UTF_8)).use { br ->
        var line = br.readLine()
        while (line != null) {
          content.append(line)
          line = br.readLine()
        }
      }
    } catch (e: IOException) {
      e.printStackTrace()
    }

    return content.toString()
  }
}